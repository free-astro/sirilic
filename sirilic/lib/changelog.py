# -*- coding: UTF-8 -*-
# ==============================================================================

# ==============================================================================
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL
# script. It can also group scripts.
# ==============================================================================
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ==============================================================================
DATE_VERSION = "September 2024"
NO_VERSION = "1.15.12"
CHANGELOG = """CHANGELOG:
    V""" + NO_VERSION + """
      + fix composition SHO
    V1.15.11
      + fix CopyLib
      + fix composition SHO
    V1.15.10
      + add composition HOO and SOO (duoband only)
      + add composition SHO
      + add composition HOO
      + fix register of duoband mode
      + fix subsky of duoband mode (multi-session)
      + fix CopyLib
      + fix RGBequa
    V1.15.9
      + add Duo Band S2/O3
      + fix wildcard replacement in file with '[...]'
      + fix for MaxOS about the siril and siril-cli path ( siril >= 1.2.1 )
      + fix fit for MaximDL 5.08
      + fix PEP8 syntaxe
      + fix FIT generated with MaximDL 5.08
    V1.15.8
      + fix "platesolving"
      + fix "weighing" only for light
      + check if Siril path is a file and file access is  "x"
      + fix coords format (Simbad) (Sometimes, the format is not degre minute second)
      + fix project history
    V1.15.7
      + add platesolve
      + add 'find text' and 'goto line' to the script editor
      + add request object coords in simbad
      + fix accent/utf-8 in windows
    V1.15.6
      + tuning for MacOs
    V1.15.5
      + menu factoring
      + add icons
    ...
"""

"""
    V1.15.4
      + fix MacOs : a internal script editor
      + fix: the path to the "About" dialog box icon.
    V1.15.3
      + fix  drizzle in duoband (Ha/O3)
      + add a internal script editor
    V1.15.2
      + add an button : 'Edit/Run'
      + fix: option findstar
      + fix: stop siril processing
    V1.15.1
      + replace the preprocess command (deprecated) with the calibrate command ( issue #26 )
      + change the compatibility version
      + new extension for compressed fit (.fit.fz)
      + fix : option drizzle with setapplyreg
    V1.15.0
      + update for siril 1.2.0-beta1
         o add new options for preprocessing
         o add new options for registring
         o add new options for stacking
         o add new options for subsky
         o add new fonction ( seqapplyreg )
         o add L/RGB compositing
      + add a dialog box to load files according to a pattern
      + improvement of the properties tab
      + fix a bug in multi-session ( issue #25 )
      + fix a bug '2pass' ( issue #24 )
    V1.14.4x
      + change about dialogbox
      + fix issue 18
      + suppress pylint warnings
    V1.14.3
      + fix archlinux compatibility
      + GUI refactoring
    V1.14.2
      + fix 'norot' register option (no prefix)
    V1.14.1
      + fix register options with multi-session
    V1.14.0
      + add register options: -transf, -norot, -minpairs
      + stop the script if there is an empty session
      + add the synthetic bias
      + delete 'hcompress' option for the compression
    V1.13.11
      + add scroll bars for low resolution screens
      + add "MAD" rejection type
      + fix linux gui
    V1.13.10
      + fix Issue #13: no debayer with cosme in  Ha or HaO3 filter (DLSR)
    V1.13.9
      + fix range for "generalized" and "percentile" rejection
    V1.13.8
      + add ExtractGreen
      + add option "weighted"
    V1.13.7
      + add a 'Siril' button in the log tab
      + delete flip option
      + Correction of the log display when no image is selected
      + fix namepath with a simple quote
      + add option "rejection type"
      + add option "identical parameters for all layers"
      + fix remove_file() with symbolic link on linux when the source file is deleted
    V1.13.6
      + fix remove_file() with symbolic link
    V1.13.5
      + add linear_match to Duo Band (HaO3)
    V1.13.4
      + fix project history
      + fix GetHDU() with compressed FITS
      + fix DuoBand HaO3  with subsky option
    V1.13.3
      + add an abort when the sequence contains an image only
      + fix objectname with space
    V1.13.2
      + fix the reading of the FITS file header (bitpix < 0 if float32)
      + fix remove_file()
    V1.13.1
      + fix version number check : siril 0.99.8.1-91f54865
    V1.13.0
      + refactoring for siril 0.99.8
      + add fix fujiX"
      + add processing summary
      + add search field in the log tab
      + add subsky sequence
      + add single-multi fit / ser mode
      + add ser processing
      + optimisation code
      + generic script without absolute path
      + fix Sirilic Step by Step (debug)
      + fix reverse project
"""
