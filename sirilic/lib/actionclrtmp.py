# -*- coding: UTF-8 -*-
'''
================================================================================
Project: SiriL-ic ( SiriL Image Companion )

This script structures the SiriL work folder into a subfolder, copies the
astronomical images into the subfolders, and builds the associated SiriL
script. It can also group scripts.
================================================================================
   Author:  M27trognondepomme <pebe92 (at) gmail.com>

This program is provided without any guarantee.

The license is  LGPL-v3
For details, see GNU General Public License, version 3 or later.
                       "https://www.gnu.org/licenses/gpl.html"
================================================================================
'''
import os
import threading

from sirilic.lib import tools
from sirilic.lib.constantes import GROUPDIR


# ==============================================================================
class ClrtmpThread(threading.Thread):
    def __init__(self, log, db, workdir):
        threading.Thread.__init__(self)
        log.clear()
        self.param = (log, db, workdir)

    # --------------------------------------------------------------------------
    def run(self):
        (log, db, workdir) = self.param
        task = tools.GestionThread()
        task.SetRunning(self)

        log.insert('\n')
        log.print_ligne('.')
        log.print_titre('.', _("DELETE TEMPORY FILE: ... "))
        log.print_ligne('.')
        log.insert('\n')

        # Calcul le nombre d'etape pour la barre de progression:
        StepCnt = 0
        for keystr, dbImage in db.GetDb().items():
            if db.Item_IsInitialised(keystr) == "uninitialized":
                continue
            for iodf in dbImage.db['Images']:
                if iodf.IsInitialised() == "":
                    continue
                StepCnt += 1
        log.SetStepNumber(StepCnt)

        # Construction de l'arborescence des dossiers
        StepCnt = 0
        log.print_ligne("-")
        log.print_titre('-', _('browse the subdirectories'))
        for keystr, dbImage in db.GetDb().items():
            objectname = dbImage.db['Object']
            layername = dbImage.db['LayerName']
            sessionname = dbImage.db['Session']
            full_dir = os.path.join(
                workdir, objectname, layername, sessionname)
            for iodf in dbImage.db['Images']:
                if task.IsAborted():
                    return
                data = iodf.GetData()
                subdir = os.path.join(full_dir, data['typename'])
                tools.nettoyage_dossier(log, subdir, None,
                                        [data['typename'].lower(),
                                         "pp_", "cc_", "r_", "bkg_", "cHaO3_",
                                         "Ha_", "OIII_", "O3_", "Green_",
                                         "cS2O3_", "S2_",
                                         "ps_"])

            tools.nettoyage_dossier(log, os.path.join(
                workdir, objectname, layername))
            tools.nettoyage_dossier(log, os.path.join(
                workdir, objectname, layername, GROUPDIR))

        log.insert('\n')
        log.print_ligne('.')
        log.print_titre('.', _("DELETE TEMPORY FILE: FINISHED"))
        log.print_ligne('.')
        log.insert('\n')
        log.SetFinishBar()
