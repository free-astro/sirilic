# -*- coding: UTF-8 -*-
'''
================================================================================
Project: SiriL-ic ( SiriL Image Companion )

This script structures the SiriL work folder into a subfolder, copies the
astronomical images into the subfolders, and builds the associated SiriL
script. It can also group scripts.
================================================================================
   Author:  M27trognondepomme <pebe92 (at) gmail.com>

This program is provided without any guarantee.

The license is  LGPL-v3
For details, see GNU General Public License, version 3 or later.
                       "https://www.gnu.org/licenses/gpl.html"
================================================================================
'''
import os
import sys
import wx
import copy
import requests
import traceback

from sirilic.lib.constantes import IMAGE, OFFSET, DARK, FLAT, DFLAT


# ==============================================================================
def conv_hhmmss(values_str):
    fields = values_str.split(" ")
    if len(fields) == 1:
        degre = float(fields)
        minute = (degre - int(degre)) * 60
        second = round((minute - int(minute)) * 60)
        minute = int(minute)
        degre = -int(degre)
    elif len(fields) == 2:
        degre = int(fields[0])
        minute = float(fields[1])
        second = round((minute - int(minute)) * 60)
        minute = int(minute)
    elif len(fields) == 3:
        degre = int(fields[0])
        minute = int(fields[1])
        second = round(float(fields[2]))
    else:
        return "coords-error:" + values_str

    return "%+02d:%02d:%02d" % (degre, minute, second)


def GetCoordObj_simbad(objectname):
    data = 'format object form1 "%%IDLIST(1):%%COO(A):%%COO(D)"\nquery id %s\nformat display' % (
        objectname, )
    resultat = requests.get(
        'https://simbad.cds.unistra.fr/simbad/sim-script?script=' + data,
        timeout=10.0)
    out = [objectname, "00:00:00", "00:00:00"]
    searcherror = "::error:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"
    searchdata = "::data::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n"
    if resultat.text.find(searcherror) == -1:
        pos = resultat.text.find(searchdata)
        if pos >= 0:
            for line in resultat.text[pos + len(searchdata):].split('\n'):
                if len(line) > 0:
                    try:
                        out = line.split(":")
                        out[1] = conv_hhmmss(out[1])
                        out[2] = conv_hhmmss(out[2])
                    except Exception:   # pylint: disable=broad-except
                        print(_("Error") + " : " + traceback.format_exc())
                        out = [objectname, _("Error") + " " + objectname, line]
    return out


# ==============================================================================
class TabProperties:
    def __init__(self, i_gui, i_prefs):

        self.CurTab = IMAGE
        self.iodf = []

        self.i_panel = i_gui.panel_1
        self.i_prop = i_gui.wproperties
        self.e_prop = self.i_prop.wMoreProp
        self.prefs = i_prefs

        # gestion manuelle des onglets car bug sur MAC OS
        self.boutons_onglet = [(self.i_prop.b_prop_image, IMAGE),
                               (self.i_prop.b_prop_offset, OFFSET),
                               (self.i_prop.b_prop_dark, DARK),
                               (self.i_prop.b_prop_flat, FLAT),
                               (self.i_prop.b_prop_dflat, DFLAT)]
        for elt in self.boutons_onglet:
            elt[0].Bind(wx.EVT_TOGGLEBUTTON, lambda evt,
                        onglet=elt[1]: self.CB_Onglet(evt, onglet))
        self.i_prop.b_prop_image.SetValue(True)

        self.e_prop.b_find.Bind(wx.EVT_BUTTON, self.CB_find_object)

        size = self.i_prop.b_bpm_file.GetSizeFromTextSize(
            self.i_prop.b_bpm_file.GetTextExtent('...'))
        self.i_prop.b_bpm_file.SetMinSize(size)

        size = self.i_prop.e_RejH.GetSizeFromTextSize(
            self.i_prop.e_RejH.GetTextExtent('0,0000'))
        if sys.platform.startswith('win32'):
            size = self.i_prop.e_RejH.GetSizeFromTextSize(
                self.i_prop.e_RejH.GetTextExtent('0,000'))

        self.i_prop.e_RejH.SetMinSize(size)
        self.i_prop.e_RejL.SetMinSize(size)
        self.i_prop.e_HotCosmetic.SetMinSize(size)
        self.i_prop.e_ColdCosmetic.SetMinSize(size)
        self.e_prop.e_sigma_findstar.SetMinSize(size)
        self.e_prop.e_roundness_findstar.SetMinSize(size)
        self.e_prop.wSubSky.spDegree.SetMinSize(size)
        self.e_prop.wSubSky.spSamples.SetMinSize(size)
        self.e_prop.wSubSky.spTolerance.SetMinSize(size)
        self.e_prop.wSubSky.spSmooth.SetMinSize(size)
        self.e_prop.t_limitmag.SetMinSize(size)

        self.e_prop.sp_pixelsize.SetMinSize(self.e_prop.sp_pixelsize.GetSizeFromTextSize(
            self.e_prop.sp_pixelsize.GetTextExtent('00.00')))
        self.e_prop.sp_focal.SetMinSize(self.e_prop.sp_focal.GetSizeFromTextSize(
            self.e_prop.sp_focal.GetTextExtent('30000')))
        self.e_prop.t_coord.SetMinSize(self.e_prop.t_coord.GetSizeFromTextSize(
            self.e_prop.t_coord.GetTextExtent('+00:00:00.00,+00:00:00.00')))

        xx = self.i_prop.wRejectFilt_stack.e_fwhm_filter
        size = xx.GetSizeFromTextSize(xx.GetTextExtent('000%'))

        self.wrf_stack = self.i_prop.wRejectFilt_stack
        self.wrf_seqreg = self.e_prop.wRejectFilt_seqreg

        for wrf in (self.wrf_stack, self.wrf_seqreg):
            wrf.e_fwhm_filter.SetMinSize(size)
            wrf.e_wfwhm_filter.SetMinSize(size)
            wrf.e_round_filter.SetMinSize(size)
            wrf.e_quality_filter.SetMinSize(size)
            wrf.e_bkg_filter.SetMinSize(size)
            wrf.e_nb_stars_filter.SetMinSize(size)

        self.e_prop.e_maxstars.SetMinSize(size)
        self.e_prop.e_minpairs.SetMinSize(size)
        self.e_prop.InvalidateBestSize()
        self.e_prop.SendSizeEvent()

        cb_Stack = self.i_prop.cb_Stack
        lst_wx = (self.i_prop.cb_rejtype,
                  self.i_prop.e_RejH, self.i_prop.e_RejL)
        cb_Stack.Bind(wx.EVT_COMBOBOX, lambda evt, combo=cb_Stack,
                      list_wx=lst_wx: self.OnCHangeStack(evt, combo, list_wx))

        cb_rej = self.i_prop.cb_rejtype
        lst_wx = (self.i_prop.l_RejH, self.i_prop.e_RejH,
                  self.i_prop.l_RejL, self.i_prop.e_RejL)
        cb_rej.Bind(wx.EVT_COMBOBOX, lambda evt, combo=cb_rej,
                    list_wx=lst_wx: self.OnCHangeRejType(evt, combo, list_wx))

        wSubSky = self.e_prop.wSubSky
        wSubSky.cb_rbf.Bind(wx.EVT_CHECKBOX, lambda evt,
                            wframe=wSubSky: self.OnCHangeRBF(evt, wframe))

        self.i_prop.b_bpm_file.Bind(
            wx.EVT_TOGGLEBUTTON, self.CB_BadPixelMap_file)

        self.i_prop.bMoreProp.Bind(wx.EVT_TOGGLEBUTTON, self.CB_MoreProperties)

        self.CB_MoreProperties(None)

    # --------------------------------------------------------------------------
    def CB_find_object(self, _event):
        with wx.TextEntryDialog(None, _('object name'), _("Find Objet in SIMBAD"), '', wx.OK | wx.CANCEL) as dlg:
            if dlg.ShowModal() == wx.ID_OK:
                objectname = dlg.GetValue()
                res = GetCoordObj_simbad(objectname)
                if len(res) == 3:
                    self.e_prop.t_coord.SetValue(res[1] + "," + res[2])
                else:
                    self.e_prop.t_coord.SetValue(
                        objectname + ":" + _("not found"))
            dlg.Destroy()
    # --------------------------------------------------------------------------

    def CB_MoreProperties(self, _event):
        toggle = self.i_prop.bMoreProp.GetValue()
        self.i_prop.wMoreProp.Show(toggle)
        self.i_panel.SendSizeEvent()

    # --------------------------------------------------------------------------
    def CB_BadPixelMap_file(self, _event):
        typefile = "Bad Pixels Map files |*.bpm|All file|*.*"
        flag = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST
        defaultdir = os.path.join(self.prefs.Get('workdir'), "Config")
        with wx.FileDialog(None, "Open", defaultdir, "", typefile, flag) as openFileDialog:
            if openFileDialog.ShowModal() == wx.ID_CANCEL:
                return
            self.i_prop.e_bpm_file.SetValue(openFileDialog.GetPath())
    # --------------------------------------------------------------------------

    def CB_Onglet(self, _event, onglet):
        self.GetGui()
        if onglet != -1:
            self.CurTab = onglet
        self.SetGui()
        for elt in self.boutons_onglet:
            elt[0].SetValue(elt[1] == self.CurTab)
        self.i_prop.InvalidateBestSize()
        self.i_prop.SendSizeEvent()

    # --------------------------------------------------------------------------
    def OnCHangeRBF(self, _event, wFrame):
        bValid = wFrame.cb_rbf.GetValue()
        wFrame.l_Degree.Show(not bValid)
        wFrame.spDegree.Show(not bValid)
        wFrame.l_Smooth.Show(bValid)
        wFrame.spSmooth.Show(bValid)
        wFrame.InvalidateBestSize()
        wFrame.SendSizeEvent()

    # --------------------------------------------------------------------------
    def OnCHangeStack(self, _event, stack, list_wx):
        (rejtype, rejH, rejL,) = list_wx
        bValid = stack.GetValue() == "mean"
        rejtype.Enable(bValid)
        if bValid and rejtype.GetValue() == "No rejection":
            bValid = False
        rejH.Enable(bValid)
        rejL.Enable(bValid)

    def OnCHangeRejType(self, event, rejtype, list_wx):
        (lrejH, rejH, lrejL, rejL,) = list_wx
        bValid = rejtype.GetValue() != "No rejection"
        rejH.Enable(bValid)
        rejL.Enable(bValid)

        # do not overwrite existing values
        if (event.GetEventObject() is None):
            return
        htext = _("High")
        ltext = _("Low")
        digits = 1
        vmax = 10.0
        vmin = 0.1
        vdefaultH = 3.0
        vdefaultL = 3.0

        if rejtype.GetValue() == "Generalized":
            htext = _("Outliers")
            ltext = _("Significance")
            digits = 3
            vmax = 0.999
            vmin = 0.001
            vdefaultH = 0.300
            vdefaultL = 0.050
        if rejtype.GetValue() == "Percentile":
            digits = 3
            vmax = 0.999
            vmin = 0.001
            vdefaultH = 0.200
            vdefaultL = 0.100

        rejL.SetDigits(digits)
        rejH.SetDigits(digits)

        rejL.SetMax(vmax)
        rejH.SetMax(vmax)

        rejL.SetMin(vmin)
        rejH.SetMin(vmin)

        rejH.SetValue(vdefaultH)
        rejL.SetValue(vdefaultL)

        lrejH.SetLabel(htext + ": ")
        lrejL.SetLabel(ltext + ": ")
        lrejH.GetParent().Layout()

    # display message if IsSameProperties == True
    def DisplayMode(self, IsSameProperties):
        if IsSameProperties:
            self.i_prop.l_mode_param.Show()
        else:
            self.i_prop.l_mode_param.Hide()

    # --------------------------------------------------------------------------
    # charge les proprietes dans l'onglet
    def Set(self, iodf, IsSameProperties):
        self.DisplayMode(IsSameProperties)
        self.copie(iodf)
        self.SetGui()

    def copie(self, iodf):
        images = []
        for elt in iodf:
            a = elt.GetData()
            images.append(copy.deepcopy(a))
        self.iodf = images

    def SetGui(self):
        select = self.CurTab
        img_prop = self.iodf[self.CurTab]

        cond = select != IMAGE
        if cond:
            self.i_prop.cb_cpylib.SetValue(img_prop["pp"]["copylib"])
        self.i_prop.cb_cpylib.Show(cond)

        if select != OFFSET:
            self.i_prop.cb_OffsetSub.SetValue(img_prop["pp"]["suboffset"])
        self.i_prop.cb_OffsetSub.Show(select != OFFSET)

        img_stack = img_prop["stack"]
        self.i_prop.cb_Stack.SetValue(img_stack["type"])
        self.i_prop.cb_Norm.SetValue(img_stack["norm"])

        img_rej = img_stack["reject"]
        self.i_prop.cb_rejtype.SetValue(img_rej["type"])
        self.i_prop.e_RejH.SetValue(img_rej["high"])
        self.i_prop.e_RejL.SetValue(img_rej["low"])

        cond = select == IMAGE
        if cond:
            self.i_prop.cb_Weighing.SetValue(img_stack["weighing"])
            self.i_prop.cb_RGBequa.SetValue(img_stack["RGBequa"])
            self.i_prop.cb_optimdark.SetValue(img_stack["DarkOpt"])

            img_cosm = img_prop["cosmetic"]
            self.i_prop.cb_Cosmetic.SetValue(img_cosm["enable"])
            self.i_prop.e_HotCosmetic.SetValue(img_cosm["hot"])
            self.i_prop.e_ColdCosmetic.SetValue(img_cosm["cold"])
            self.i_prop.e_bpm_file.SetValue(img_cosm["bpm"])

            img_find = img_prop["findstar"]

            self.e_prop.e_sigma_findstar.SetDigits(2)
            self.e_prop.e_roundness_findstar.SetDigits(2)
            self.e_prop.e_pixel_findstar.SetDigits(2)
            self.e_prop.e_minA_findstar.SetDigits(2)
            self.e_prop.e_maxA_findstar.SetDigits(2)
            self.e_prop.e_convergence_findstar.SetDigits(0)
            self.e_prop.e_focal_findstar.SetDigits(0)

            self.e_prop.cb_findstar.SetValue(img_find["enable"])
            self.e_prop.e_sigma_findstar.SetValue(img_find["sigma"])
            self.e_prop.e_roundness_findstar.SetValue(img_find["roundness"])
            self.e_prop.e_radius_findstar.SetValue(img_find["radius"])
            self.e_prop.e_focal_findstar.SetValue(img_find["focal"])
            self.e_prop.e_pixel_findstar.SetValue(img_find["pixelsize"])
            self.e_prop.e_convergence_findstar.SetValue(
                img_find["convergence"])
            self.e_prop.cb_solver_findstar.SetValue(img_find["solver"])
            self.e_prop.e_minbeta_findstar.SetValue(img_find["minbeta"])
            self.e_prop.cb_relax_findstar.SetValue(img_find["relax"])
            self.e_prop.e_minA_findstar.SetValue(img_find["minA"])
            self.e_prop.e_maxA_findstar.SetValue(img_find["maxA"])

            img_reg = img_prop["register"]
            self.e_prop.cb_RegLayer.SetValue(img_reg["layerstar"])
            self.e_prop.cb_transf.SetValue(img_reg["transfo"])
            self.e_prop.cb_PixInterpo.SetValue(img_reg["interpo"])
            self.e_prop.cb_norot.SetValue(img_reg["norot"])
            self.e_prop.cb_RegDrizzle.SetValue(img_reg["drizzle"])
            self.e_prop.e_minpairs.SetValue(img_reg["minpairs"])
            self.e_prop.e_maxstars.SetValue(img_reg["maxstars"])
            self.e_prop.cb_2pass.SetValue(img_reg["2pass"])
            self.e_prop.cb_noout.SetValue(img_reg["noout"])
            self.e_prop.cb_noclamp.SetValue(img_reg["noclamp"])
            self.e_prop.cb_selected.SetValue(img_reg["selected"])
            self.e_prop.cb_nostarslist.SetValue(img_reg["nostarslist"])

            img_sreg = img_prop["seqreg"]
            self.e_prop.cb_SeqRegistering.SetValue(img_sreg["enable"])
            self.e_prop.cb_autoframing.SetValue(img_sreg["autoframing"])
            self.e_prop.cb_SeqInterpo.SetValue(img_sreg["interpo"])
            self.e_prop.cb_SeqDrizzle.SetValue(img_sreg["drizzle"])
            self.e_prop.cb_SeqLayer.SetValue(img_sreg["layerstar"])
            self.e_prop.cb_SeqNoclamp.SetValue(img_sreg["noclamp"])

            self.setfilter(self.wrf_stack, img_stack)
            self.setfilter(self.wrf_seqreg, img_sreg)

            img_subsky = img_prop["subsky"]
            self.e_prop.wSubSky.spDegree.SetValue(img_subsky["degree"])
            self.e_prop.wSubSky.cb_rbf.SetValue(img_subsky["rbf"])
            self.e_prop.wSubSky.spSamples.SetValue(img_subsky["samples"])
            self.e_prop.wSubSky.spTolerance.SetValue(img_subsky["tolerance"])
            self.e_prop.wSubSky.spSmooth.SetValue(img_subsky["smooth"])

            img_platesolve = img_prop["platesolve"]
            self.e_prop.rb_platesolve.SetSelection(img_platesolve["mode"])
            self.e_prop.sp_pixelsize.SetValue(img_platesolve["pixelsize"])
            self.e_prop.sp_focal.SetValue(img_platesolve["focal"])
            self.e_prop.t_coord.SetValue(img_platesolve["center"])
            self.e_prop.cb_noflip.SetValue(img_platesolve["noflip"])
            self.e_prop.cb_downscale.SetValue(img_platesolve["downscale"])
            self.e_prop.cb_localasnet.SetValue(img_platesolve["localasnet"])
            self.e_prop.t_limitmag.SetValue(img_platesolve["limitmag"])
            self.e_prop.lb_catalog.SetValue(img_platesolve["catalog"])
            self.e_prop.cb_forceplatesolve.SetValue(img_platesolve["force"])

        self.i_prop.panel_moreprop.Show(cond)

        self.i_prop.cb_fix_fujix.Show(cond)
        self.i_prop.cb_RGBequa.Show(cond)
        self.i_prop.cb_optimdark.Show(cond)
        self.i_prop.cb_Weighing.Show(cond)
        self.i_prop.l_weighing.Show(cond)

        self.i_prop.cb_Cosmetic.Show(cond)
        self.i_prop.e_HotCosmetic.Show(cond)
        self.i_prop.e_ColdCosmetic.Show(cond)
        self.i_prop.e_bpm_file.Show(cond)
        self.i_prop.b_bpm_file.Show(cond)
        self.i_prop.l_HotCosmetic.Show(cond)
        self.i_prop.l_ColdCosmetic.Show(cond)
        self.i_prop.l_bpm_file.Show(cond)

        self.wrf_stack.Show(cond)
        evt = wx.CommandEvent()
        evt.SetEventType(wx.EVT_COMBOBOX.typeId)

        wx.PostEvent(self.i_prop.cb_Stack, evt)
        wx.PostEvent(self.i_prop.cb_rejtype, evt)

    def setfilter(self, gui_filter, img_stack):
        img_filt = img_stack['filter']
        gui_filter.e_fwhm_filter.SetValue(img_filt["fwhm"])
        gui_filter.e_wfwhm_filter.SetValue(img_filt["wfwhm"])
        gui_filter.e_round_filter.SetValue(img_filt["round"])
        gui_filter.e_quality_filter.SetValue(img_filt["quality"])
        gui_filter.e_bkg_filter.SetValue(img_filt["bkg"])
        gui_filter.e_nb_stars_filter.SetValue(img_filt["nbstars"])

    # --------------------------------------------------------------------------
    # recupere les proprietes de l'onglet
    def GetGui(self):
        select = self.CurTab
        img_prop = self.iodf[select]

        if select != IMAGE:
            img_prop["pp"]["copylib"] = self.i_prop.cb_cpylib.GetValue()

        if select != OFFSET:
            img_prop["pp"]["suboffset"] = self.i_prop.cb_OffsetSub.GetValue()

        img_stack = img_prop["stack"]
        img_stack["type"] = self.i_prop.cb_Stack.GetValue()
        img_stack["norm"] = self.i_prop.cb_Norm.GetValue()

        if select == IMAGE:
            img_stack["RGBequa"] = self.i_prop.cb_RGBequa.GetValue()
            img_stack["weighing"] = self.i_prop.cb_Weighing.GetValue()
            img_stack["DarkOpt"] = self.i_prop.cb_optimdark.GetValue()

        img_rej = img_stack["reject"]
        img_rej["type"] = self.i_prop.cb_rejtype.GetValue()
        img_rej["high"] = self.i_prop.e_RejH.GetValue()
        img_rej["low"] = self.i_prop.e_RejL.GetValue()

        if select == IMAGE:
            img_prop["fix_fujix"] = self.i_prop.cb_fix_fujix.GetValue()

            img_stack["filter"] = self.getFilter(self.wrf_stack)

            img_cosm = img_prop["cosmetic"]
            img_cosm["enable"] = self.i_prop.cb_Cosmetic.GetValue()
            img_cosm["hot"] = self.i_prop.e_HotCosmetic.GetValue()
            img_cosm["cold"] = self.i_prop.e_ColdCosmetic.GetValue()
            img_cosm["bpm"] = self.i_prop.e_bpm_file.GetValue()

            img_find = img_prop["findstar"]
            img_find["enable"] = self.e_prop.cb_findstar.GetValue()
            img_find["sigma"] = self.e_prop.e_sigma_findstar.GetValue()
            img_find["roundness"] = self.e_prop.e_roundness_findstar.GetValue()
            img_find["radius"] = self.e_prop.e_radius_findstar.GetValue()
            img_find["focal"] = self.e_prop.e_focal_findstar.GetValue()
            img_find["pixelsize"] = self.e_prop.e_pixel_findstar.GetValue()
            img_find["convergence"] = self.e_prop.e_convergence_findstar.GetValue()
            img_find["solver"] = self.e_prop.cb_solver_findstar.GetValue()
            img_find["minbeta"] = self.e_prop.e_minbeta_findstar.GetValue()
            img_find["relax"] = self.e_prop.cb_relax_findstar.GetValue()
            img_find["minA"] = self.e_prop.e_minA_findstar.GetValue()
            img_find["maxA"] = self.e_prop.e_maxA_findstar.GetValue()

            img_reg = img_prop["register"]
            img_reg["layerstar"] = self.e_prop.cb_RegLayer.GetValue()
            img_reg["transfo"] = self.e_prop.cb_transf.GetValue()
            img_reg["interpo"] = self.e_prop.cb_PixInterpo.GetValue()
            img_reg["norot"] = self.e_prop.cb_norot.GetValue()
            img_reg["drizzle"] = self.e_prop.cb_RegDrizzle.GetValue()
            img_reg["minpairs"] = self.e_prop.e_minpairs.GetValue()
            img_reg["maxstars"] = self.e_prop.e_maxstars.GetValue()
            img_reg["2pass"] = self.e_prop.cb_2pass.GetValue()
            img_reg["noout"] = self.e_prop.cb_noout.GetValue()
            img_reg["noclamp"] = self.e_prop.cb_noclamp.GetValue()
            img_reg["selected"] = self.e_prop.cb_selected.GetValue()
            img_reg["nostarslist"] = self.e_prop.cb_nostarslist.GetValue()

            img_sreg = img_prop["seqreg"]
            img_sreg["enable"] = self.e_prop.cb_SeqRegistering.GetValue()
            img_sreg["autoframing"] = self.e_prop.cb_autoframing.GetValue()
            img_sreg["interpo"] = self.e_prop.cb_SeqInterpo.GetValue()
            img_sreg["drizzle"] = self.e_prop.cb_SeqDrizzle.GetValue()
            img_sreg["layerstar"] = self.e_prop.cb_SeqLayer.GetValue()

            img_sreg["filter"] = self.getFilter(self.wrf_seqreg)

            img_subsky = img_prop["subsky"]
            img_subsky["degree"] = self.e_prop.wSubSky.spDegree.GetValue()
            img_subsky["rbf"] = self.e_prop.wSubSky.cb_rbf.GetValue()
            img_subsky["samples"] = self.e_prop.wSubSky.spSamples.GetValue()
            img_subsky["tolerance"] = self.e_prop.wSubSky.spTolerance.GetValue()
            img_subsky["smooth"] = self.e_prop.wSubSky.spSmooth.GetValue()

            img_platesolve = img_prop["platesolve"]
            img_platesolve["mode"] = self.e_prop.rb_platesolve.GetSelection()
            img_platesolve["pixelsize"] = self.e_prop.sp_pixelsize.GetValue()
            img_platesolve["focal"] = self.e_prop.sp_focal.GetValue()
            img_platesolve["center"] = self.e_prop.t_coord.GetValue()
            img_platesolve["noflip"] = self.e_prop.cb_noflip.GetValue()
            img_platesolve["downscale"] = self.e_prop.cb_downscale.GetValue()
            img_platesolve["localasnet"] = self.e_prop.cb_localasnet.GetValue()
            img_platesolve["limitmag"] = self.e_prop.t_limitmag.GetValue()
            img_platesolve["catalog"] = self.e_prop.lb_catalog.GetValue()
            img_platesolve["force"] = self.e_prop.cb_forceplatesolve.GetValue()

    def getFilter(self, gui_filter):
        img_filt = {}
        img_filt["fwhm"] = gui_filter.e_fwhm_filter.GetValue()
        img_filt["wfwhm"] = gui_filter.e_wfwhm_filter.GetValue()
        img_filt["round"] = gui_filter.e_round_filter.GetValue()
        img_filt["quality"] = gui_filter.e_quality_filter.GetValue()
        img_filt["bkg"] = gui_filter.e_bkg_filter.GetValue()
        img_filt["nbstars"] = gui_filter.e_nb_stars_filter.GetValue()
        return img_filt

    # --------------------------------------------------------------------------
    # recupere les proprietes de l'onglet
    def GetImages(self, typename):
        self.GetGui()
        return self.iodf[typename]
