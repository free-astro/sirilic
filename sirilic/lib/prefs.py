# -*- coding: UTF-8 -*-
'''
================================================================================
Project: SiriL-ic ( SiriL Image Companion )

This script structures the SiriL work folder into a subfolder, copies the
astronomical images into the subfolders, and builds the associated SiriL
script. It can also group scripts.
================================================================================
   Author:  M27trognondepomme <pebe92 (at) gmail.com>

This program is provided without any guarantee.

The license is  LGPL-v3
For details, see GNU General Public License, version 3 or later.
                       "https://www.gnu.org/licenses/gpl.html"
================================================================================
'''
import os
from sirilic.lib import tools


class CPrefs:
    def __init__(self, dirname, filename):
        self.filenamerc = filename
        self.dirnamerc = dirname
        tools.mkdirs(self.dirnamerc)
        self.Default()

    def GetDir(self):
        return self.dirnamerc

    def Get(self, field):
        return self.data[field]

    def Set(self, field, value):
        self.data[field] = value

    def Default(self):
        self.data = {}
        self.data['offsetmaster'] = ""
        self.data['darkmaster'] = ""
        self.data['workdir'] = os.path.join(self.dirnamerc, "Siril", "Work")
        self.data['sirilexe'] = ""
        self.data['siril_autorun'] = True
        self.data['cleandir'] = True
        self.data['links'] = True
        self.data['debug'] = False
        self.data['workdirreturn'] = True
        self.data['nbcpu'] = 0
        self.data['nbmem'] = 0
        self.data['last_project'] = []
        self.data['siril_dev'] = False
        self.data['seqmode'] = 0
        self.data['savetif'] = False
        self.data['float32b'] = False
        self.data['compress'] = False
        self.data['compress_type'] = "rice"
        self.data['compress_quantif'] = 16
        self.data['root_pattern'] = ''
        self.data['files_pattern'] = ['offset/*', 'dark/*',
                                      'dflat/*', 'flat/*',
                                      'light/*']

    def Load(self, filename=None):
        if filename is None:
            filename = os.path.join(self.dirnamerc, self.filenamerc)
        if not os.path.isfile(filename):
            return
        with open(filename, "r", encoding="utf-8") as fd:
            lines = fd.readlines()
            self.Default()
            try:
                prefs = tools.String2data(lines)
                for key in self.data.keys():
                    # ne charge que les cles existantes dans self.data()
                    if not (key in prefs):
                        continue  # la cle n'existe pas, on passe a la suite
                    self.data[key] = prefs[key]
            except Exception as e:   # pylint: disable=broad-except
                filename = os.path.join(self.dirnamerc, self.filenamerc)
                print("*** CPrefs::Load() error loading " +
                      filename + " :: " + str(e))

    def Save(self, filename=None):
        if filename is None:
            filename = os.path.join(self.dirnamerc, self.filenamerc)
        chaine = tools.Stringify(self.data)
        with open(filename, "w", encoding="utf-8") as fd:
            fd.write("# Don't edit the file \n")
            fd.write(chaine)
