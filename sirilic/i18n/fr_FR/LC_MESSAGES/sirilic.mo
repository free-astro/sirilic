��    �     T  �  �      �(     �(     �(  @   �(     )     )     )     )     %)     +)     1)     8)     >)     D)     J)     S)  �  Y)  C  7.     {/     /  *   �/  1   �/     �/     �/     �/  "   �/     0     0     +0     D0     J0     _0     g0     ~0  	   �0     �0  (   �0  I   �0     (1     /1  /   @1     p1  �  }1     3     -3  	   43     >3  7   J3     �3     �3     �3     �3     �3     �3     �3  -   �3     4  9   /4  !   i4     �4  �  �4     '6     -6     =6     F6     a6     e6     l6  /   �6     �6  #   �6  t   �6  6   d7     �7     �7  	   �7     �7  	   �7     �7  �  �7  �   �9     :      :     ):  8   =:     v:     z:     :  	   �:     �:     �:     �:     �:  
   �:     �:     �:  #   �:     ;     #;     3;     M;     k;  )   �;     �;     �;     �;      �;     �;     <  4   ,<     a<     j<     v<     �<     �<     �<     �<     �<  !   �<     �<  1   �<     '=     .=     C=     I=     a=     z=     �=     �=     �=     �=     �=     �=     �=     �=     �=     >     >  	   &>     0>     9>  +   ?>  \   k>  }  �>     F@  !   R@     t@  	   y@     �@     �@     �@     �@     �@     �@     �@     �@     �@     A     A     A     A     $A     )A     0A  �   6A  s  �A    QC  �   XF  `   �F     :G     JG  �   QG     H     	H     )H  !   7H     YH     jH     H  4   �H     �H     �H     �H     �H     �H  
    I     I     I  3   I  7   II     �I     �I     �I  	   �I     �I     �I     �I     �I     �I     �I     �I     �I     �I     �I     �I     J     #J     2J     GJ     WJ     kJ     yJ     �J     �J     �J     �J     �J     �J     �J     �J     �J     �J     �J     �J     K     	K     K     &K     +K     :K     ?K  
   PK     [K  
   dK     oK     uK  
   �K     �K     �K     �K     �K     �K     �K  
   �K  	   �K     �K     �K     L     L  <   )L     fL     jL     xL     }L     �L     �L     �L     �L     �L     �L     �L     �L     M  	   M     M     'M     -M     /M     AM     FM     VM  -   [M     �M  :   �M     �M  %   �M  4   	N     >N  
   XN  �  cN     �O     P     P     P     ,P  /   9P  0   iP  0   �P  )   �P  [  �P     QW     WW     dW     hW     {W     �W     �W     �W  �  �W     �Y     �Y  #   �Y  !   �Y  %   �Y  *   Z     1Z     FZ     `Z     eZ     kZ     tZ     �Z  2   �Z  &   �Z     �Z     �Z      �Z  �   [  �   �[  �   �\  �  �]  �   �_  2   `     N`     ]`  _   b`     �`     �`     �`     �`  @   �`  
   &a  
   1a     <a     Ka  ,   \a  J   �a     �a     �a  �   �a     �b     �b     �b     c     c     c     "c     'c     3c     Mc     Uc     ^c     bc     ic  &   oc     �c     �c     �c     �c     �c     �c  <   �c  '   d     Cd  	   Qd     [d  6  cd  �   �e  �   �f  ~   .g  �   �g  �   =h  �   �h     �i     �i     �i     �i  
   �i     �i     �i     �i  D   �i     +j     <j     Sj     Xj     aj     gj  
   mj     xj  #   ~j     �j  �   �j  �   �k  %   Sl     yl     �l     �l     �l     �l     �l     �l  K   �l     .m     6m     Bm     Nm     Zm     fm     km     rm  
   �m     �m  
   �m     �m  ?   �m     �m     �m     �m     �m      n     n  ?   n     Wn     kn     tn     {n     �n     �n     �n     �n     �n     �n     �n     �n  	   �n     �n     �n     �n  	   �n  \   �n  %   Bo     ho     oo  H   {o  ]   �o     "p  
   *p     5p     ;p     Dp     Qp     Xp  �   ^p     q  '   q     4q     9q  	   Hq     Rq     [q     cq     uq     ~q     �q  
   �q     �q     �q  
   �q     �q     �q     �q     �q  O   r  0   Sr  J   �r  �   �r  ;   \s     �s  D   �s  
   �s     �s     �s     t  &   t     Dt     Zt  ?  rt     �u     �u  I   �u     v     v     v      v     )v     0v     8v     @v     Iv     Rv     Xv  	   hv  N  rv  o  �|     1~     5~  C   A~  @   �~     �~     �~     �~  )   �~                    4     =     W     _     ~     �     �  .   �  i   �     W�     ^�  0   y�     ��  �  ��     ��     ��  
   Ƃ     т  >   ݂     �     ,�     =�     P�     Y�     ^�     r�  5   ��  (   Ã  A   �  $   .�     S�  �  m�     Y�     e�     t�     z�     ��     ��  !   ��  3   Ć     ��  (   �  �   <�  .   ć     �     ��     �     �     +�     7�  �  K�  y   $�     ��  
   ��     ��  =   Ɗ     �     �     �  	   �     "�  *   .�  .   Y�     ��  
   ��     ��     ��  1   ��     �     �     �     (�  &   E�  A   l�     ��     ��     ͌  %   �     �     1�  F   J�     ��     ��     ��     ��     ��     č     ɍ     �  "   ��     "�  6   ;�     r�     z�     ��     ��     ��  '   ю  #   ��     �     $�     8�     M�     R�     Y�     n�     v�     �     ��     ��     ��     Ï  5   ɏ  �   ��  �  ��     U�     a�     w�     }�     ��     ��     ��     ��     Ò     ܒ     �     �     �     #�     3�     :�     @�     E�     J�     Q�  �   W�  �   �  �  ��  �   ��  e   C�     ��     ��  �   ��     ��     ��     ��      ��     �     �     �  >   �  &   Y�     ��     ��  	   ��     ��     ��     ��     ��  6   ��  (   ��     �     #�     '�  	   -�     7�     P�     S�     W�     ^�     s�     ��     ��     ��     ��     ��     ͝     ߝ     �     �     #�     =�     K�     X�     d�     l�     {�     ��     ��     ��     ��     ��     Ȟ     ۞     ޞ     �     �      �     �     �     (�     /�  
   J�     U�     Z�     g�  #   n�  
   ��  &   ��     ğ     ҟ  	   �     �      �     �  
   �     �     $�     4�     D�  <   Y�     ��     ��     ��  
   ��     Ġ     �  
   ��     �     �     ,�     K�     Y�  	   k�     u�     ��     ��     ��     ��     ��     á     ӡ  9   ١     �  F   #�     j�  )   ��  4   ��     �     �  �  �     �     2�     F�     N�     `�  :   r�  J   ��  :   ��  6   3�  Q  j�     ��  
   ­     ͭ     ѭ     �     ��     �      �    5�     M�     S�  #   a�  #   ��  )   ��  *   Ӱ     ��     �     /�  
   A�  
   L�     W�     m�  ;   t�  1   ��     �  	   ��  &   ��  �  %�  <  ��  �   �  j  ҵ  �   =�  C   ��     9�     H�  k   Q�     ��     ٹ     ߹  	   �  J   �  
   <�  
   G�     R�     e�  5   z�  V   ��     �     �  -  '�     U�     ^�     e�     ��     ��     ��     ��     ��     ��  	   ۼ     �     ��     �     ��  -   �     5�     A�     a�     g�     o�     |�  C   ��  =   Ƚ     �  	   �     �  }  %�    ��  �   ��  �   {�  �   �  �   ��  �   }�     [�     k�     r�     ��     ��  #   ��     ��     ��  F   ��     $�     8�     R�     W�     `�     f�  
   l�     w�  /   ��     ��    ��  �   ��  )   ��     ��     ��      �     �     "�     3�     D�  b   U�     ��     ��     ��     ��     ��     ��     ��  "   ��  
    �     +�  
   /�     :�  G   @�     ��     ��     ��     ��     ��     ��  G   ��      �     (�     1�     8�     =�     A�     J�     R�     Z�     ]�     m�     u�     |�     ��     ��     ��     ��  j   ��  ,   �     G�     M�  O   \�  |   ��     )�     /�     ?�     E�     Q�     `�     f�  �   l�  	   M�  5   W�     ��     ��     ��     ��  	   ��     ��     ��     ��     ��  
   ��     �     �      �  	   )�     3�  #   7�     [�  P   z�  6   ��  G   �  �   J�  C   ��     8�  Q   ;�     ��     ��  -   ��     ��  2   ��     �     ,�     ^      [  Y  3  Q   Q  �      S  4   �  P   ~      �   �  p       �      6     :      �   )       X  �       ,  �   �  �  3       �  �  �   ]       N      �       c  "  I  �  �   �   8   �   �   �   �  d   �       �   k     w   �    l  �  -       e  I           r  !  �  �       
    �   �   z   �  �   �      �     �   $   W   *       �  H                               �       y  X   �   �   ?     �   �       �   �   f   {       �       @   h  o   �     �                  7      >  �   �           b   �       �   �   �  ;       j   �              J   �   �   �  }          �          (  �           �   �      �  �  �   v      M   b  �   2   �   �   �      ]  �               �  �  �   �  7   Z      z     _     q         V   �         '   �  i      �          i  �  �   �   ,   G      �      d          �   9   '  �  5   �  n  B   �      �   �  	      �   �   �  #         k  O   K      �  r   �           l   �               A   M  �   �      u   1       U  W  �       %  �   _   5  �      [   4      $         �   <    �   �     �      x      �       �   Z   �       \         q   �  �               .   �   �      �  n   �   B  L                  �           �   �       �   �  m   =   �  <   �   �  �   0  �  v   �     �  �         C           ~     �  =  a             �   &   �      �                             �   �       (   �           �          �   �   �     V  �  u    �      +   |   P      �   �  E    �   F  %   .  	   @  L      +              �       R  �      �           �  �           �         �      D   �   �     p      6      T  e      �   F       �  �    J        �  |  �  `  �  �   ?   �   �   c   >   D  �       w  �  R   /        �  /   )      �     �  �   �   �  �   �       �  �  �   �   �  9  m  �    �   �  �   a         �  �  t   &     `   �  {  �     �                   �    �      1  U   �        �   �  �   K   �  x               �          g  f  #  
   t      �   �     O  N           8  g                  y   �  �   �  !   G   �     "   }  h   �  �   �   �    �  o  �      *  �       �  �  �   �   �  �   ^   �     T       �       s      H  �   �  C  �  2  �   �       �      �   �  �   :     �  �       �  �       �      E   �             \   �      A  ;              �      �                       �   j  Y   s   �                   -      0   �     �  �   S   �         ...   of   radius  of the initial search box and must be between 3 and 50. &About &Copy &Cut &Find &Goto &Open &Paste &Quit &Redo &Save &Save as &Undo 'Sum': This is the simplest method.
Pixel values are added in an image normalized to 32-bit values.
It increases signal/noise ratio (SNR) as square root of number of images.

'Mean' with rejection: This method is used to reject deviant pixels iteratively.
Seven methods have been implemented:
- Percentile Clipping
- Sigma Clipping
- MAD Clipping
- Median Sigma Clipping
- Winsorized Sigma Clipping
- Linear Fit Clipping
- Generalized Extreme Studentized Deviate Test
After pixel rejection, the mean of the stack is computed.

'Median': This method was mostly used for dark/flat/offset stacking although now we recommend the use of average with rejection stacking.
The median value of the pixels in the stack is computed for each pixel.
As this method should only be used for dark/flat/offset stacking, it does not take
into account shifts computed during registration.

'Pixel maximum': This case is mainly used to construct long exposure star-trails images.
Pixels of the image are replaced by pixels at the same coordinates if intensity is greater.

'Pixel minimum': This case could be useful to select the area to be cropped after the stack operation.
Pixels of the image are replaced by pixels at the same coordinates if intensity is lower. 'max' (bounding box) adds a black border around each image as required  so that no part of the image is cropped when registered.

'min' (common area) crops each image to the area it has in common with all images of the sequence.

'cog' determines the best  framing position as the center of gravity (cog) of all the images. ... ... aborted 0=default  of SiriL else ratio of used mem 0=default value  of SiriL else number of used cpu 2 pass 85% 90% A simple Script editor in Sirilic. ADD Abort Action Abort the current action About About  Script Editor Actions Add a file in the list Add a light to the project Add light After Registering After executing the script, launch Siril Align the result images, small shifts and chromatic aberrations can occur Append Apply platesolve Are you sure you want to overwrite image files? Auto framing Automatic framing of the output sequence can be specified using:

o max: (bounding box) adds a black border around each image as  required so that no part of the image is cropped when registered.

o min : (common area) crops each image to the area it has in common with all images of the sequence.
                    
o cog: determines the best framing position as the center of gravity (cog) of all the images. Automating: layer  BIASES BIASES :  Background: Bad Pixel Map to specify which pixels must be corrected Bad Pixel Map:  Before Registering Biases subtract Biases: Blue Blue [debayer] Build Siril script Build directories and copy or link the images Build partially siril script Build the Siril script and launch the image preprocessing Build the Siril script of project Building directories By default, Siril uses IKSS estimators of location and scale to compute normalisation. For long sequences, computing these estimators can be quite intensive. For such cases, you can opt in for faster estimators (based on median and median absolute deviation). While less resistant to outliers in each image, they can still give a satisfactory result when compared to no normalization at all CLEAR COPY : FINISHED COPY THE COPYING TO SIRIL DIRECTORY CPU Cancel Check for active processing Check if the "Developer Mode" option is enabled Choose Master File Choose Siril executable (full path) Clamping of the bicubic and lanczos4 interpolation methods is the default, 
to avoid artefacts, but can be disabled. Clean the object directory before to launch the script Cleaning :  Clear Clear all Clear all files Color CCD Compatibility with Computes a synthetic background gradient using either the polynomial function 
model of 'degree' degrees or the RBF model (if '-rbf' is provided instead) 
and subtracts it from the image. The number of samples per horizontal line and 
the tolerance to exclude brighter areas can be adjusted with the optional arguments. 
Tolerance is in mad units: median + tolerance * mad. 
For RBF, the additional smoothing parameter is also available Computes the level of the local sky background thanks to a polynomial function of an order degree and subtracts it from the image Copy Copy all Cosmetic correction Create a new project to astronomical image preprocessing Cut DARK DARK :  DARK FLAT DELETE DELETE TEMPORY FILE: ...  DELETE TEMPORY FILE: FINISHED DSLR Dark-Flat: Dark: Debayerised DSLR Debug ( save stdout/stderr in file) Default Default masters Delete a file of the list Delete a light of the project Delete intermediate files Delete intermediate files from subfolders Delete light Disable Display more properties Do you save the current project? Don't edit : generated by  Don't edit the file Drag and drop the images or use the selection button Drizzle  Duo Ha/Oiii Duo Sii/Oiii EDIT EXPERT MODE Edit Edit Siril script Edit a file in the list Edit and  modify the siril script Edit properties Edit the properties and parameters of the project Editor Enable Siril dev ... Error Error: Siril don't work Error: siril don't exist Error: siril isn't a executable Error: siril isn't a file Exit Exit the editor. FITS compression FLAT FWHM:  Fast normalization File Files Final image Find Objet in SIMBAD Find text Finished Flat: For RBF, the additional smoothing parameter For faster star detection in big images, downsampling the image is possible with -downscale. Four methods of weighing are available:

'Number of stars' weighs individual frames based on number of stars computed during registration step.

'Weighted FWHM' weighs individual frames based on wFWHM computed during registration step.

'Noise' weighs individual frames based on background noise values.

'Number of images' weighs individual frames based on their integration time. Generalized Global properties for the project Goto Goto line Green Green [debayer] Group Directory H alpha H alpha / Oiii [color] H alpha [color] H alpha [debayer] H beta H beta [debayer] HOO compositing Halpha Hbeta Help High High:  IMAGE If Moffat is selected, 'minbeta=' defines the minimum value of beta for which  candidate stars will be accepted and must be greater or equal to 0.0 and less than 10.0 If WCS or other image metadata is erroneous or missing, arguments must be passed: the approximate image center coordinates can be provided in decimal degrees or degree/hour minute second values (J2000 with colon separators), with right ascension and declination values separated by a comma or a space (not mandatory for astrometry.net).
e.g. for NGC281 : 0:52:59,56:37:19 If one of these items is selected, a normalisation process will be applied to all input
images before stacking.

- Normalisation matches the mean background of all input images, then, the
normalisation is processed by multiplication or addition. Keep in mind that both
processes generally lead to similar results but multiplicative normalisation is
prefered for image which will be used for multiplication or division as flat-field.

- Scale matches dispersion by weighting all input images. This tends to improve
the signal-to-noise ratio and therefore this is the option used by default with
the additive normalisation.

** The offset and dark masters should not be processed with normalisation.
However, multiplicative normalisation must be used with flat-field frames ** If the image has already been plate solved nothing will be done, unless the -platesolve argument is passed to force a new solve. If you check this button, the channels in the final image will be equalized (color images only). Image Directory Image: Images can be either plate solved by Siril using a star catalog and the global registration algorithm or by astrometry.net's local solve-field command (enabled with -localasnet). Info Information about this program. Interpolation LIBRARY INTO THE MASTER DIRECTORY LRGB compositing Last processed image Last project Launch part of the processing script for fine tuning Launch the image preprocessing Layer Layers Light Light/*.fits Linear-Fit Link Load Load a  project to astronomical image preprocessing Load a last project to astronomical image preprocessing Log Low Low:  Luminance Luminance [debayer] M1 MAD MASTER MASTER BUILDING MEM Make directory: Median Merge Min bêta : Modify a light of the project Modify light Modify project Modify the file path More properties Multi-image by FITS Multi-session Nb of images Nb of stars New New project Next ocurrence No rejection Noise None Normalisation Number of session Number stars: OK OSC/CCD Object Object Directory Object name Oiii Oiii [debayer] Open Open a new file. Optim.Dark Outliers PROCESSING Paste Pattern Files Brower Percentile Platesolving Preferences Preprocessing Process Process list Project Properties Quality:  RBF: RGB compositing RGB equalization RGB/LRGB compositing Recreate a project from the directory of an existing project Red Red [debayer] Redo Register Register CCD Layers Register OSC Layers Registering Rejecting Filter Rejection type Renorm O3 to  Replace all Reverse Project Round:  Run Siril Run Siril script Run:  S SCRIPT : FINISHED SER  SHO compositing Save Save a copy in tif format (16b, uncompressed) Save as Save as a new  project to astronomical image preprocessing Save the current file. Save the file under a different name. Save the project to astronomical image preprocessing Script building: FINISHED Select All Select the set of images to be stacked:

'FWHM': images with best computed FWHM (star-based registration only)

'weighted FWHM': this is an improvement of a simple FWHM. It allows to exclude much more spurious images by using the number of stars detected compared to the reference image (star-based registration only)

'roundness': images with best star roundness (star-based registration only)
 Sequence Registering Sequence mode: Session Session basename Session name Set biases and dark masters for all the project Set the default SiriL executable according to OS Set the default value of biases and dark masters Setting the default Bias and Dark Masters Seven rejection algorithms are available:

'Percentile Clipping' is a one-step rejection algorithm ideal for small sets of data (up to 6 images).

'Sigma Clipping' is an iterative algorithm which will reject pixels whose distance from median will be farthest than two given values in sigma units.

'MAD Clipping' is an iterative algorithm working as Sigma Clipping except that the estimator used is the Median Absolute Deviation (MAD). This is generally used for noisy infrared image processing.

'Median Sigma Clipping' is the same algorithm except than the rejected pixels are replaced by the median value.

'Winsorized Sigma Clipping' is very similar to Sigma Clipping method but it uses an algorithm based on Huber's work [1] [2].

'Linear Fit Clipping' is an algorithm developed by Juan Conejero, main developer of PixInsight [2]. It fits the best straight line (y=ax+b) of the pixel stack and rejects outliers. This algorithm performs very well with large stack and images containing sky gradients with differing spatial distributions and orientations.

The 'Generalized Extreme Studentized Deviate Test' algorithm [3] is a generalization of Grubbs Test that is used to detect one or more outliers in a univariate data set that follows an approximately normal distribution. This algorithm shows excellent performances with large dataset of more 50 images.

[1] Peter J. Huber and E. Ronchetti (2009), Robust Statistics, 2nd Ed., Wiley
[2] Juan Conejero, ImageIntegration, Pixinsight Tutorial
[3] Rosner, Bernard (May 1983), Percentage Points for a Generalized ESD Many-Outlier Procedure, Technometrics, 25(2), pp. 165-172 Sigma Significance Sii Sii / Oiii [color] Sii [color] Sii [debayer] Single image by FITS SiriL executable :  SiriLic ( SiriL Image Converter) is a software for preparing
acquisition files (raw, Offset, Flat and Dark) for processing with SiriL software.

It does 4 things:

 1. Structuring the SiriL working directory into sub-folders
 2. Convert Raw, Offset, Dark or Flat files into SiriL sequence
 3. Automatically generate the SiriL script according to the files present and the options
 4. Preprocess directly the images

It can also process the multi-session

 Siril Siril = x.y.z Siril IC  ( SiriL Image Converter ) Siril is compatible with Sirilic  Siril is not compatible with Sirilic  Siril requires a minimum of 2 light images Sirilic preferences  Sirilic requires : Siril  Skip Stack Stacking Star finding Status Step 1: Build Directories and copy/link the images Step 2: Build and Run the Siril script SubSky Summary Symbolic Link Error on Windows:  The choice of the star catalog is automatic if  -catalog=None option is passed
If the option is passed, it forces the use of the remote catalog given in argument, with possible values: tycho2, nomad, gaia, ppmxl, brightstars, apass The limit magnitude of stars used for plate solving is automatically computed from the size of the field of view, but can be altered by passing a +offset or -offset value to -limitmag=, or simply an absolute positive value for the limit magnitude. The option activates the sub-pixel stacking by up-scaling by 2 the generated images or by setting a flag that will proceed to the up-scaling during stacking if -transf=shift -noout are passed The option specifies the type of transformation to compute:
o shift : a 2 degrees of freedom (dof), X and Y translation, rigid transform (can be used with -noout to not create a new sequence, and still be stacked directly)
o similarity: a 4 dof transform, X and Y translation, a scale and rotation
o affine: a 6 dof transform, X and Y translation, two scales, one shear and rotation
o homography (default): a 8 dof warping transform
*** empty = default value of siril *** The registration is done on the first layer for which data exists for RGB images unless specified option (0, 1 or 2 for R, G and B respectively. Timout expired : Problem to run 'Siril --version'  Transformation Undo Unless -noflip is specified, if the image is detected as being upside-down, it will be flipped. Unselect All WFWHM WFWHM:  Warning Warning, Symbolic Link on Windows works only with NTFS partition Weighing:  Winsorized Work Directory Work directory:  action => intialize or delete empty sessions activates up-scaling by 2 the images created in the transformed 
sequence. add add the image files adds a preliminary pass to the registration, first detecting stars and choosing the best reference frame using a function based on FWHM and the number of stars, then computing the transforms of all frames relative to this new reference. addscale affine amplitude limit of stars and flip if required apass area auto brightstars browse the subdirectories catalog choice 1 cog cold:  color configure the solver model to be used. convergence copy the library by Siril cubic current debayerised degree: detection duoband : 32-bit forcing for pixmath normalization disables saving the star lists to disk. does'nt exist downscale drizzle during image transformation, the pixel interpolation method can be specified. If none is passed, the transformation is forced to shift and a pixel-wise shift is applied to each image without any interpolation. This option is unused if the output sequence is not created. 
*** empty = default value of siril *** empty = default value of siril
an integer that specify the maximum number of star to find within each frame (must be between 100 and 2000). With more stars, a more accurate registration can be computed, but will take more time to run. empty = default value of siril
an integer that specify the minimum number of star pairs a frame must have with the reference frame, otherwise the frame will be dropped. empty = no filter
value : filter  on the  fwhm value (ex: fwhm < 4.2 )
pourcentage : filter xx % on the best  fwhm (ex:  95% ) empty = no filter
value : filter  on the  weighted fwhm value (ex: fwhm < 4.2 )
pourcentage : filter xx % on the best weighted fwhm (ex:  95% ) empty = no filter
value [0.0-1.0] : filter  on the roundness value (ex: roundness > 0.8 )
pourcentage : filter xx % on the best  roundness (ex:  95% )
 empty =no filter 
value [0.0-1.0] : filter  on the quality value (ex: quality > 0.8 )
pourcentage : filter xx % on the best  quality (ex:  95% )
** for the planetary only **
 empty history enable equalize (CFA) error find &Next find object coordinates fix Fuji X     focal focal length in  millimeter
0 = takes the focal length from the file force platesolve full name of siril exe gaia gaussian gzip1 gzip2 homography hot:  identical parameters for all layers image center images with best number of stars detected (star-based registration only)
empty = no filter
value : filter  on the  value (ex: number > 42 )
pourcentage : filter xx % on the best number of stars detected (ex:  95% )
 images with lowest background values (star-based registration only)
empty = no filter
value : filter  on the  value (ex: background < 4.2 )
pourcentage : filter xx % on the lowest background (ex:  95% )
 intermediate stacking (multi-session) lanczos4 last processed image last project 0 last project 1 last project 2 last project 3 last project 4 launches Siril to do the last treatments (color calibration, gradient, ...) layer 0 layer 0 (R) layer 1 (G) layer 2 (B) limit  mag. line linear load files with pattern localasnet max max  stars max : maximum amplitude of stars to keep, normalized between 0 and 1. mean median merge min min : min.  star pairs minimum amplitude of stars to keep, normalized between 0 and 1. mode: float 32 bits modified moffat mono mul mulscale nearest next no no clamping no flip no out no rotate no stars list nomad none not found number of iterations performed to fit PSF and should be set between 1 and 3 (more tolerant). number of samples per horizontal line object object name option equalizes the mean intensity of RGB layers of the CFA flat master path to your Bad Pixel Map to specify which pixels must be corrected
*** empty = no file **** pattern pixel size ppmxl previous quantization radius relax relaxes the checks that are done on star candidates to assess if they are stars or not, to allow objects not shaped like stars to still be  accepted (off by default) results return to the workdir at the script end rice root directory roundness samples: search: select a BPM file selected shift sigma similarity sirilic status: Ready skipping smoothing: solver:  sum task already in progress the CCD is a debayerised DSLR the focal length of the telescope (mm)
0 = takes the focal length from the file the option excludes images will not be processed the pixel size of the sensor. (µm)
0 = takes the pixel size from the file the tolerance to exclude brighter areas can be adjusted with the optional arguments. 
Tolerance is in mad units: 
median + tolerance * mad.
 threshold above noise and must be greater or equal to 0.05. to to use symbolic link on  the windows 10 , select the developper mode tolerance: tycho2 uncomment next line to force uninitialized use symbolic links ( replace the copy) waiting a few seconds work directory of SiriL Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-08-13 23:14+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.3.1
X-Poedit-SourceCharset: UTF-8
  ...   de   rayon du champ de recherche initial et doit être compris entre 3 et 50. A propos &Copier &Couper &Trouver &Aller &Ouvrir &Coller &Quitter &Refaire Sauve Sauvegarde sous &Défaire "Somme" : Il s'agit de la méthode la plus simple.
Les valeurs des pixels sont additionnées dans une image normalisée à des valeurs de 32 bits.
Elle augmente le rapport signal/bruit (SNR) comme racine carrée du nombre d'images.

moyenne avec rejet : Cette méthode est utilisée pour rejeter les pixels déviants de manière itérative.
Sept méthodes ont été mises en œuvre :
- Écrêtage par centile
- Écrêtage Sigma
- Écrêtage MAD
- Écrêtage Sigma médian
- Écrêtage Sigma winsorisé
- Découpage par ajustement linéaire
- Test de l'écart généralisé extrême studentisé
Après le rejet des pixels, la moyenne de la pile est calculée.

médiane : Cette méthode était surtout utilisée pour l'empilage sombre/plat/décalé bien que maintenant nous recommandons l'utilisation de la moyenne avec l'empilage par rejet.
La valeur médiane des pixels de la pile est calculée pour chaque pixel.
Comme cette méthode ne doit être utilisée que pour l'empilage sombre/plat/offset, elle ne prend pas en compte les décalages calculés lors de l'enregistrement
ne prend pas en compte les décalages calculés lors de l'enregistrement.

pixel maximum : Ce cas est principalement utilisé pour construire des images de pistes stellaires à longue exposition.
Les pixels de l'image sont remplacés par des pixels aux mêmes coordonnées si l'intensité est supérieure.

pixel minimum' : Ce cas peut être utile pour sélectionner la zone à recadrer après l'opération d'empilage.
Les pixels de l'image sont remplacés par des pixels situés aux mêmes coordonnées si l'intensité est inférieure. max" (bounding box) ajoute une bordure noire autour de chaque image afin qu'aucune partie de l'image ne soit coupée lors de l'enregistrement.

min " (zone commune) recadre chaque image dans la zone qu'elle a en commun avec toutes les images de la séquence.

cog' détermine la meilleure position de cadrage en tant que centre de gravité (cog) de toutes les images. ... … Annuler 0=valeur par défaut de SiriL sinon ratio de mémoire max utilisée 0=valeur par défaut de SiriL sinon nombre de cœur CPU utilisé 2 passes 85% 90% Un simple éditeur de scripts en Sirilic. Ajout Arrêt d'urgence Arrêt de l'action en cours A propos A propos de Script Editor Actions Ajout d'un fichier à la liste Ajouter une image au projet Ajouter une image Après alignement Après avoir exécuté le script, lancez Siril Aligner les images résultantes, de petits décalages et des aberrations chromatiques peuvent se produire Ajoute Résolution astrométrique Etes-vous sûr de vouloir d'écraser les images? Cadrage auto Le cadrage automatique de la séquence de sortie peut être spécifié en utilisant :

o max : (bounding box) ajoute une bordure noire autour de chaque image selon les besoins afin qu'aucune partie de l'image ne soit coupée lors de l'enregistrement.

o min : (zone commune) recadre chaque image sur la zone qu'elle a en commun avec toutes les images de la séquence.
                    
o cog : détermine la meilleure position de cadrage comme centre de gravité (cog) de toutes les images. Automatisation : Couche  OFFSETS OFFSETS :  Background: 'Bad Pixel Map' spécifie quels pixels doivent être corrigés Bad Pixel Map:  Avant alignement Soustrait l'offset Offsets: Bleu Bleu [débayerisé] Construire le script Siril Construction des dossiers et copie ou link des images Construire partiellement le script siril Construire le script Siril et lancer le prétraitement de l'image Construire le script Siril du projet Construction des dossiers Par défaut, Siril utilise les estimateurs IKSS de localisation et d'échelle pour calculer la normalisation. Pour les longues séquences, le calcul de ces estimateurs peut être assez intensif. Dans ce cas, vous pouvez opter pour des estimateurs plus rapides (basés sur la médiane et la déviation absolue médiane). Bien que moins résistants aux valeurs aberrantes de chaque image, ils peuvent néanmoins donner un résultat satisfaisant par rapport à l'absence totale de normalisation Efface tous COPIE TERMINEE Copie COPIE DANS LE DOSSIER SIRIL CPU Annuler Cocher pour activer le traitement Vérifiez si l'option "Developer Mode" est activée Choisir le fichier maître Sélectionne le fichier executable SiriL Le clamping de l'interpolation bicubic ou lanczos4 est activé par défaut mais pour éviter des artefacts , il peut être désactivé. Nettoye le  dossier avant de  lancer le script Nettoyage:  Efface Efface
tout Efface tous les fichiers CCD Couleur Compatibilité avec Calcule un gradient synthétique en utilisant soit la fonction polynomiale
de 'degré' degrés ou le modèle RBF (si '-rbf' est fourni à la place)
et le soustrait de l'image. Le nombre d'échantillons par ligne horizontale et
la tolérance pour exclure les zones plus lumineuses peuvent être ajustés avec les arguments optionnels
La tolérance est en unités mad : médiane + tolérance * mad
Pour RBF, le paramètre de lissage supplémentaire est également disponible Calcule le niveau du fond du ciel local grâce à une fonction polynomiale d'un degré d'ordre et le soustrait de l'image Copie Copie tous Correction cosmétique Création d'un nouveau projet de prétraitement d'image astro Couper DARK DARK :  DARK FLAT Suppression DESTRUCTION DES FICHIERS TEMPORAIRES: …  DESTRUCTION DES FICHIERS TEMPORAIRES: TERMINES APN Dark-Flat: Dark: APN débayérisé Debug (redirection stdout/stderr dans un fichier) Valeur par défaut Maitres par défaut Supprime un fichier de la liste Détruit une image du projet Détruire les fichiers intermédiaires Détruire les fichiers intermédiaires dans les sous-répertoires Détruit une image Désactiver Afficher plus de propriétés Voulez-vous sauver le projet courant? Ne pas éditer : généré par  Ne pas editer le fichier Glisser et déposer les fichiers ou utiliser les boutons de sélection Drizzle  Duo Ha/Oiii Duo Sii/Oiii Edit MODE EXPERT Edit Modifier le script Siril Edite un fichier de la liste Editer et modifier le script siril Edition des propriétés Edition des propriétés/paramètres globaux du projet Editeur Active SiriL Dev ... Erreur Erreur: SIRIL ne fonctionne pas Erreur: SIRIL n'existe pas Erreur : siril n'est pas un exécutable Erreur : siril n'est pas un fichier Sortir Quitter l'éditeur. Compression des FITS FLAT FWHM:  Normalisation rapide Fichier Fichiers Image finale Trouver l'objet dans SIMBAD Chercher un texte Terminé Flat: Pour le RBF, le paramètre de lissage supplémentaire Pour une détection plus rapide des étoiles dans les grandes images, il est possible de sous-échantillonner l’image avec -downscale. Quatre méthodes de pesée sont disponibles :

'Nb of stars' pondère les images individuelles en fonction du nombre d'étoiles calculé pendant l'étape d'enregistrement.

Weighted FWHM' pondère les images individuelles en fonction du wFWHM calculé pendant l'étape d'enregistrement.

'Noise' pondère les images individuelles en fonction des valeurs de bruit de fond.

'Nb images' pondère les images individuelles en fonction de leur temps d'intégration. Generalized Propriété du projet Aller Aller à la  ligne Vert Vert [débayerisé] Dossier de fusion H alpha H alpha / Oiii [couleur] H alpha [couleur] H alpha [débayerisé] H bêta H beta [débayerisé] Composition HOO Halpha Hbeta Aide Haut Haut:  IMAGE Si 'Moffat' est sélectionné, "minbeta=" définit la valeur minimale de bêta pour laquelle les étoiles candidates seront acceptées et doit être supérieure ou égale à 0,0 et inférieure à 10,0 Si les données WCS ou d'autres métadonnées de l'image sont erronées ou manquantes, des arguments doivent être ajoutés :
les coordonnées approximatives du centre de l'image peuvent être fournies en degrés décimaux ou en degrés/heure minute seconde (J2000 avec des séparateurs de deux points), avec les valeurs d'ascension droite et de déclinaison séparées par une virgule ou un espace (non obligatoire pour astrometry.net).
e.g. for NGC281 : 0:52:59,56:37:19 Si l'un de ces éléments est sélectionné, un processus de normalisation sera appliqué à toutes les images d'entrée avant l'empilage.

- La normalisation correspond à l'arrière-plan moyen de toutes les images d'entrée, puis, la
normalisation est traitée par multiplication ou addition. Gardez à l'esprit que les deux
processus conduisent généralement à des résultats similaires mais la normalisation multiplicative est préférable pour les images qui seront utilisées pour la multiplication ou la division en tant que champ plat.

- La normalisation correspond à la dispersion en pondérant toutes les images d'entrée. Cela tend à améliorer le rapport signal/bruit et c'est donc l'option utilisée par défaut avec la normalisation additive.

** Les masters offset et dark ne doivent pas être traités avec la normalisation.
Cependant, la normalisation multiplicative doit être utilisée avec les FLAT ** Si une résolution astrométrique a déjà été faite sur l'image, rien ne sera fait, sauf si l'argument -platesolve est ajouté afin de forcer une nouvelle résolution. Si vous cochez ce bouton, les canaux de l'image finale seront égalisés (images couleur uniquement). Dossier Image Image: Les images peuvent être résolues par Siril à l'aide d'un catalogue d'étoiles et de l'algorithme d'enregistrement global ou par la commande locale solve-field d'astrometry.net (activée avec -localasnet). Info Informations sur ce programme. Interpolation LIBRAIRIE DANS LE DOSSIER MAITRE Composition LRGB Dernière image traitée Dernier projet Lancer une partie du script de traitement pour un réglage fin Lancement du prétraitement des images Couche Couches Luminance Light/*.fits Linear match Lien Charge Chargement d'un projet de prétraitement d'image astro Charge un dernier des 5 derniers projets Log Bas Bas:  Luminance Luminance [débayerisé] M1 MAD MAITRE :Construction Maitre Ratio mémoire Faire les dossiers: Median Fusion Min bêta : Modifie  une image du projet Modifie une image Modifie le projet Modifie le chemin du fichier Plus de propriétés Plusieurs images par FITS Multi-session Nb of images Nb of stars Nouveau Nouveau projet Prochaine occurrence No rejection Noise None Normalisation Nombre de session Nombre d'étoiles: OK CCD/CMOS Objet Dossier de l'objet Nom de l'objet Oiii Oiii [débayerisé] Ouvrir Ouvrir un nouveau fichier. Optim.Noir Taux : Traitement Coller Explorateur de fichiers avec motifs Percentile Resolution d'étoiles par astrométrie Préférences Prétraitement Processus List des traitements Projet Propriétés Qualité:  RBF: Composition RVB Egalisation RVB Composition RVB/LRVB Recréer un projet à partie d'un dossier d'un ancien projet Rouge Rouge [débayerisé] Refaire Alignement Alignement des couches du CCD Alignement des couches OSC Alignement Rejecting Filter Type de réjection Renormalise O3 par rapport à  Remplace tous Récupère Projet Rondeur:  Exécuter Siril Exécuter le script Siril Lancement:  S SCRIPT: Terminé SER  Composition SHO Sauve Enregistrer une copie au format tif (16b, non compressé) Sauvegarde sous Sauvegarde sous un autre nom du projet de prétraitement d'image astro Sauve le fichier courant. Enregistrez le fichier sous un autre nom. Sauvegarde du projet de prétraitement d'image astro CONSTRUCTION DU SCRIPT TERMINEE Sélection de 
l'ensemble Sélectionnez le jeu d'images à empiler :

'FWHM' : images avec la meilleure FWHM calculée (enregistrement basé sur les étoiles uniquement).

'weighted FWHM' : il s'agit d'une amélioration du FWHM simple. Il permet d'exclure beaucoup plus d'images parasites en utilisant le nombre d'étoiles détectées par rapport à l'image de référence (enregistrement basé sur les étoiles uniquement).

roundness' : images avec la meilleure rondeur d'étoiles (enregistrement basé sur les étoiles uniquement).
 Alignement de sequence Mode des Sequences: Session Nom de la session Nom de la session Définir les maîtres Offset et Dark pour tous les projets Met l'exécutable SiriL par défaut en fonction du système d'exploitation Définir la valeur par défaut des maîtres Offset et Dark Définir l'offset maitre et le dark maitre par défaut Sept algorithmes de rejet sont disponibles :

'Percentile Clipping' est un algorithme de rejet en une étape, idéal pour les petits ensembles de données (jusqu'à 6 images).

'Sigma Clipping' est un algorithme itératif qui rejette les pixels dont la distance à la médiane est supérieure aux deux valeurs données en unités sigma.

'MAD Clipping' est un algorithme itératif fonctionnant comme Sigma Clipping, sauf que l'estimateur utilisé est la déviation absolue médiane (MAD). Il est généralement utilisé pour le traitement d'images infrarouges bruyantes.

'Median Sigma Clipping' est le même algorithme sauf que les pixels rejetés sont remplacés par la valeur médiane.

Le "Winsorized Sigma Clipping" est très similaire à la méthode Sigma Clipping mais il utilise un algorithme basé sur le travail de Huber [1] [2].

'Linear Fit Clipping' est un algorithme développé par Juan Conejero, principal développeur de PixInsight [2]. Il ajuste la meilleure ligne droite (y=ax+b) de la pile de pixels et rejette les valeurs aberrantes. Cet algorithme fonctionne très bien avec de grandes piles et des images contenant des gradients de ciel avec des distributions spatiales et des orientations différentes.

L'algorithme 'Generalized Extreme Studentized Deviate Test' [3] est une généralisation du test de Grubbs qui est utilisé pour détecter une ou plusieurs valeurs aberrantes dans un ensemble de données univariées qui suit une distribution approximativement normale. Cet algorithme montre d'excellentes performances avec un grand ensemble de données de plus de 50 images.

[1] Peter J. Huber et E. Ronchetti (2009), Robust Statistics, 2e édition, Wiley.
[2] Juan Conejero, ImageIntegration, Tutoriel Pixinsight.
[3] Rosner, Bernard (mai 1983), Percentage Points for a Generalized ESD Many-Outlier Procedure, Technometrics, 25(2), pp. 165-172 Sigma Importance Sii Sii / Oiii [couleur] Sii [couleur] Sii [débayerisé] Une seule image par FITS Exécutable SiriL :  SiriLic ( SiriL Image Converter)  est un logiciel de préparation 
des fichiers d’acquisition (brutes, Offset, Flat et Dark) pour les traiter avec 
le logiciel SiriL.

Il fait  4 choses :

1. Structuration du répertoire de travail SiriL en sous-dossier
2. Convertir les fichiers Brute, Offset, Dark ou Flat en séquence SiriL
3. Générer automatiquement le script SiriL en fonction des fichiers présents et des options
4. Prétraiter directement les images 

Il peut aussi traiter plusieurs objets à la fois et le multi-session

 Siril Siril = x.y.z Siril IC  ( SiriL Image Converter ) Siril est  compatible avec Sirilic  Siril n'est  pas compatible avec Sirilic  Siril exige un minimum de 2 images (light) Préférences de Sirilic  Sirilic exige : Siril  Passe à la suite Empilement Empilement Détection d'étoiles Status Etape 1: Construction des dossiers et copie/link des images Etape 2 : Construire et exécuter le script Siril SubSky (gradient) Synthèse Erreur de lien symbolique **Windows**  Le choix du catalogue d'étoiles est automatique sauf si l'option -catalog= est utilisée : si des catalogues locaux sont installés, ils seront utilisés, sinon le choix est basé sur le champ de vision et la magnitude limite. Si l'option est donnée, elle force l'utilisation du catalogue distant donné en argument, avec les valeurs possibles : tycho2, nomad, gaia, ppmxl, brightstars, apass La magnitude limite des étoiles utilisées pour la résolution astrométrique et le PCC est automatiquement calculée à partir de la taille du champ de vision, mais peut être modifiée en passant une valeur +offset ou -offset à -limitmag=, ou simplement avec une valeur absolue positive pour la magnitude limite. L'option active l'empilement des sous-pixels en augmentant de 2 l'échelle des images générées ou en mettant un drapeau qui procédera à l'augmentation d'échelle pendant l'empilement si -transf=shift -noout sont passés L'option spécifie le type de transformation à calculer :
o shift : une transformation rigide à 2 degrés de liberté, une translation X et Y (peut être utilisée avec -noout pour ne pas créer une nouvelle séquence, et être empilée directement)
o similarity : une transformation à 4 degrés de liberté, une translation X et Y, une échelle et une rotation
o affine : une transformation de 6 degrés de liberté, translation X et Y, deux échelles, un cisaillement et une rotation
o homographie (par défaut) : une transformée de distorsion de 8 degrés de liberté
*** empty = valeur par défaut de siril *** L'enregistrement est effectué sur la première couche pour laquelle des données existent pour les images RVB, sauf si l'option spécifiée (0, 1 ou 2 pour R, G et B respectivement). Expiration du délai: problème pour exécuter «Siril --version»  Transformation Défaire Quand 'noflip' est non coché , l'image sera retournée si elle
 est détectée comme étant à l’envers. Désélection
de l'ensemble WFWHM WFWHM:  Attention Attention, le lien symbolique sur Windows fonctionne que la partition NTFS Weighing:  Winsorized Dossier de travail Dossier de travail:  action => initialiser ou supprimer les sessions vides active la mise à l'échelle par 2 des images créées dans la séquence transformée. add ajoute les fichiers d'image ajoute une passe préliminaire à l'enregistrement, en détectant d'abord les étoiles et en choisissant le meilleur cadre de référence à l'aide d'une fonction basée sur FWHM et le nombre d'étoiles, puis en calculant les transformées de tous les cadres par rapport à cette nouvelle référence. addscale affine limite d'amplitude des étoiles et retourne si nécessaire apass zone auto brightstars parcours les sous-répertoires catalogue choix 1 cog sigma froid:  couleur configurer le modèle de solveur à utiliser. convergence copie de la librairie par SiriL cubic courant débayerisé degré: détection duoband : forçage 32 bits pour la normalisation pixmath désactive la sauvegarde des listes d'étoiles sur le disque. n'existe pas downscale drizzle pendant la transformation de l'image, la méthode d'interpolation des pixels peut être spécifiée. Si aucune méthode n'est passée, la transformation est forcée de se décaler et un décalage par pixel est appliqué à chaque image sans aucune interpolation. Cette option est inutilisée si la séquence de sortie n'est pas créée. 
*** empty = valeur par défaut de siril *** empty = valeur par défaut de siril
un nombre entier qui spécifie le nombre maximum d'étoiles à trouver dans chaque image (doit être compris entre 100 et 2000). Avec plus d'étoiles, un enregistrement plus précis peut être calculé, mais prendra plus de temps à exécuter. vide = la valeur par défaut de siril
un entier qui spécifie le nombre minimal de paires d’étoiles qu’une image doit avoir avec le cadre de référence, sinon l’image sera supprimée. vide = pas de filtrage
une valeur : filtrage sur la valeur de fwhm  (ex: fwhm < 4.2 )
un pourcentage: filtre xx% des meilleures valeurs de fwhm (ex: 95%) vide = pas de filtrage
une valeur : filtrage sur la valeur pondérée de fwhm  (ex: fwhm < 4.2 )
un pourcentage: filtre xx% des meilleures valeurs  pondérées de fwhm (ex: 95%) vide = pas de filtrage
une valeur entre 0.0 et 1.0  : filtrage sur la valeur de rondeur (ex:  rondeur > 0.8 )
un pourcentage: filtre xx% des meilleures valeurs de rondeur (ex: 95%)
 vide = pas de filtrage
une valeur entre 0.0 et 1.0  : filtrage sur la valeur de qualité (ex:  qualité > 0.8 )
un pourcentage: filtre xx% des meilleures valeurs de qualité (ex: 95%)
** pour du planétaire uniquement **
 historique vide active égaliser (CFA) erreur trouver le suivant trouver les coordonnées d'un objet correction Fuji X     focale focale en millimètre
0 = la focale est est celle contenu dans l'image force la resolution nom complet de SiriL(exe) gaia gaussian gzip1 gzip2 homography sigma chaud:  propriétés identiques pour toutes les couches centre image images avec le meilleur nombre d'étoiles détectées (enregistrement basé sur les étoiles uniquement)
vide = pas de filtre
valeur : filtre sur la valeur (ex : number > 42 )
pourcentage : filtre xx % sur le meilleur nombre d'étoiles détectées (ex : 95% )
 images avec les valeurs de fond les plus faibles (enregistrement basé sur les étoiles uniquement)
vide= pas de filtre
valeur: filtre sur la valeur (ex : background < 4.2 )
pourcentage : filtre xx % sur le fond le plus bas (ex : 95% )
 empilement intermédiaire (multi-session) lanczos4 dernière image traitée dernier projet 0 dernier projet 1 dernier projet 2 dernier projet 3 dernier projet 4 lancement de SiriL pour faire les derniers traitements (étalonnages des couleurs, gradients, …) layer 0 layer 0 (R) layer 1 (G) layer 2 (B) limit  mag. ligne linear charger des fichiers avec un motif localasnet max max  stars max : amplitude maximale des étoiles à conserver, normalisée entre 0 et 1. mean median fusion min min : min. de paires d'étoiles amplitude minimale des étoiles à conserver, normalisée entre 0 et 1. mode : 32 bits virgule flottante modifié moffat mono mul mulscale nearest suivant no pas de clamping no flip no out sans rotation no stars list nomad none non trouvé nombre d'itérations effectuées pour ajuster le PSF et devrait être fixé entre 1 et 3 (plus tolérant). nombre d'échantillons par ligne horizontale objet nom de l'objet option: égalise l'intensité moyenne des couches RVB de PLU (flat) maitre  CFA chemin d'accès à votre 'Bad Pixel Map' pour spécifier quels pixels doivent être corrigés
*** vide = pas de fichier **** motif taille du pixel ppmxl précédent quantification rayon relax assouplit les vérifications qui sont faites sur les candidats étoiles pour évaluer si ce sont des étoiles ou non, pour permettre aux objets qui n'ont pas la forme d'une étoile d'être acceptés (désactivé par défaut) resultats retour dans le dossier de travail à la fin du script rice dossier racine rondeur échantillons: chercher: sélection du fichier BPM sélectionné shift sigma similarity status: prêt passe au suivant lissage: solveur:  sum tâche déjà en cours d'exécution le CCD est un APN débayerisé focale du télescope en millimètre
0 = la focale est celle contenu dans l'image l'option exclut les images qui ne seront pas traitées la taille du photosite
0 = la taille est est celle contenu dans l'image la tolérance pour exclure les zones plus lumineuses peut être ajustée avec les arguments optionnels. 
La tolérance est en unités mad : médiane + tolérance * mad.
 seuil au-dessus du bruit et doit être supérieur ou égal à 0,05. en pour utiliser le lien symbolique sous windows, sélectionner le mode développeur tolérance: tycho2 décommenter la ligne suivante pour forcer le non-initialisé utilise des liens symboliques ( remplace la copie) attendre quelques secondes dossier de travail de SiriL 