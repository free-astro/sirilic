# README (EN)

## SIRILIC

**SiriLic** ( **SiriL**'s **I**nteractif **C**ompanion) is a software for preparing 
acquisition files (raw, Biases, Flat and Dark) for processing with SiriL software.

It does three things:

1. Structuring the SiriL working directory into sub-folders
2. Convert Raw, Biases , Dark or Flat files into SiriL sequence
3. Automatically generate the SiriL script according to the files present and the options

Sirilic allows also to batch process multiple channel and sessions. 

A user manual is available here : [wiki sirilic](https://gitlab.com/free-astro/sirilic/-/wikis/home)

# LISEZ-MOI (FR)

## SIRILIC

**SiriLic** ( **SiriL**'s **I**nteractif **C**ompanion) est un logiciel de préparation 
des fichiers d’acquisition (brutes, Offset, Flat et Dark) pour les traiter avec 
le logiciel SiriL.

Il fait  3 choses :

1. Structuration du répertoire de travail SiriL en sous-dossier
2. Convertir les fichiers Brute, Offset, Dark ou Flat en séquence SiriL
3. Générer automatiquement le script SiriL en fonction des fichiers présents et des options

Sirilic permet de traiter en lot plusieurs couches et sessions.

Un manuel utilisateur  est disponible ici :  [wiki sirilic](https://gitlab.com/free-astro/sirilic/-/wikis/home)
